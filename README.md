# Prom Docker

A docker image with prometheus and loki packaged in a envoy container to provide
http basic auth

## Usage

```yaml
version: '2'
services:
  prom:
    build: registry.gitlab.com/adeattwood/docker-prom
    environment:
      PASSWORD: testing
    networks:
      - network
  grafana:
    image: grafana/grafana:latest
    ports:
      - "3000:3000"
    networks:
      - network

networks:
  network:
    driver: bridge
```

Then in your Grafana you can add a "Prometheus" and "Loki" data source with the
host to your instance on port `8000` the default user name is "prom" this can be
configured with the "USERNAME" environment variable.

```
Host: http://prom:8000
Username: prom
Password: testing
``
