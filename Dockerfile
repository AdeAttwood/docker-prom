FROM envoyproxy/envoy:v1.18.2

RUN apt-get update && apt-get install -y --no-install-recommends \
    tar \
    unzip \
    curl \
    supervisor \
    gettext \
    ca-certificates

RUN mkdir -p /src/config /data

ENV PROMETHEUS_URL="https://github.com/prometheus/prometheus/releases/download/v2.26.0/prometheus-2.26.0.linux-amd64.tar.gz"
ENV PROMETHEUS_SHA256="8dd6786c338dc62728e8891c13b62eda66c7f28a01398869f2b3712895b441b9"

RUN set -eux; \
    curl -fsSL -o prometheus.tar.gz ${PROMETHEUS_URL}; \
	echo "$PROMETHEUS_SHA256 *prometheus.tar.gz" | sha256sum -c -; \
    tar -xf prometheus.tar.gz -C /src; \
    mv /src/prometheus-* /src/prometheus; \
    rm ./prometheus*

ENV LOKI_URL="https://github.com/grafana/loki/releases/download/v2.2.1/loki-linux-amd64.zip"
ENV LOKI_SHA256="dacfb229dbc7064b1d6390173ea6963eb3c85f60dc2336081b0113476405c5aa"

RUN set -eux; \
    curl -fsSL -o loki.zip ${LOKI_URL}; \
	echo "$LOKI_SHA256 *loki.zip" | sha256sum -c -; \
    unzip loki.zip; \
    mv loki-linux-amd64 /src/loki; \
    rm ./loki*

COPY ./config /src/config
COPY ./config/docker-entrypoint.sh /docker-entrypoint.sh
RUN chown -R envoy:envoy /src /data

COPY config/supervisor.conf /etc/supervisor/conf.d/supervisor.conf
RUN mkdir /var/run/supervisor && chown envoy:envoy /var/run/supervisor && chmod 755 /var/run/supervisor
RUN chown envoy:envoy /var/log/supervisor && chmod 755 /var/log/supervisor

CMD ["supervisord"]
